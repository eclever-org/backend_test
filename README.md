# Aufgabe

Im Folgenden geht es um den Entwurf und die Umsetzung einer Schnittstelle.Die Hauptfunktion ist das anlegen, bearbeiten, anzeigen und löschen von Entwickler-Entitäten über eine Schnittstelle. Der Datensatz soll auch persistiert werden.

Die Umsetzung beinhaltet unterschiedliche Schwerpunkte aus verschiedenen Bereichen, die im Zusammenspiel eine Anwendung ergeben.

## Vorgabe Technologien
- Typescript 
- Node.js 
- Express.js oder Nest.js 
- Docker und Docker-Compose 
- Beliebige Datenbank (bspw. Redis, SQL, MongoDB …)
- Der Rest ist frei wählbar

## Vorgabe Datenmodelle
### Entwickler-Modell
- Id
- Name 
- E-Mail 
- Level
	- Senior oder Junior

## Schwerpunkte 
Die Umsetzung gliedert sich in mehrere Teilbereiche:

- Umsetzung der Schnittstelle
- Konfiguration der Umgebungen
- Persistenz der Daten 
- Orchestrierung

## Umsetzung der Schnittstelle

- Rückgabe aller Entwickler     	
	- Filterung nach dem Level     
- Rückgabe eines bestimmten Entwicklers     
- Anlegen eines Entwickler mit dem oben definierten Entwickler-Modell 
- Bearbeiten eines Entwicklers 
- Löschen eines Entwicklers

## Konfiguration der Umgebungen
- Es soll eine Konfiguration verwendet werden um zwischen den Umgebungen zu unterscheiden 
- Dabei sind folgende Umgebungen vorgegeben:
	- Development
	- Production

## Persistenz der Daten
Die Daten sollen jeweils pro Umgebung unterschiedlich gespeichert werden 

- Für Development:
	- In-Memory Persistenz
	- Diese soll selbst umgesetzt werden
- Für Production:
	- Beliebige Datenbank 

## Orchestrierung 
Die Datenbank (bspw. redis) soll in docker-compose integriert werden

```yaml
version: '3.6'
services:
    redis:
      image: 'bitnami/redis:latest'
      ports:
        - 6379:6379
      environment:
          - ALLOW_EMPTY_PASSWORD=yes
```

